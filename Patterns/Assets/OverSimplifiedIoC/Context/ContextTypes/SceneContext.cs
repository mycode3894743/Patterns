using IOC.Base.Context.Abstraction;
using UnityEngine.SceneManagement;

namespace IOC.Base.Context
{
	/// <summary>
	/// Scene related context, exist while scene exist
	/// </summary>
	public abstract class SceneContext : MinorContext
	{
		/// <summary>
		/// Attached scene
		/// </summary>
		private Scene _attachedScene = default;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="scene">attached scene</param>
		/// <param name="parent">parent context</param>
		public SceneContext(Scene scene, BaseContext parent) : base(parent)
		{
			_parentContext = parent;
			_attachedScene = scene;
			SceneManager.sceneUnloaded += OnUnloadScene;

			InitContainer();
		}

		/// <summary>
		/// On scene unload
		/// </summary>
		/// <param name="scene">unloaded scene</param>
		private void OnUnloadScene(Scene scene)
		{
			if (scene == _attachedScene)
			{
				Dispose();
			}
		}

		/// <inheritdoc/>
		public override void Dispose()
		{
			base.Dispose();
			SceneManager.sceneUnloaded -= OnUnloadScene;
		}
	}
}
