using System;
using System.Collections.Generic;
using System.Linq;
using IOC.Base.Context.Abstraction;
using UnityEngine;

namespace IOC.Base.Containers
{
	/// <summary>
	/// Base ioc container
	/// </summary>
	public class Container : IDisposable
	{
		/// <summary>
		/// List of registered objects
		/// </summary>
		private readonly Dictionary<Type, Type> _registeredObjects = new();
		private readonly Dictionary<Type, object> _buildedObjects = new();

		/// <summary>
		/// Link to parent context
		/// TODO: not the best way
		/// </summary>
		private BaseContext _parentContext;

		/// <summary>
		/// Register parent context
		/// </summary>
		/// <param name="parentContext">parent context</param>
		internal void AddParentContext(BaseContext parentContext)
		{
			_parentContext = parentContext;
		}

		internal void BuildRegistered()
		{
			foreach (var registered in _registeredObjects)
			{
				if (!_buildedObjects.ContainsKey(registered.Key))
				{
					_buildedObjects.Add(registered.Key, GetInstance(registered.Value));
				}
			}
		}

		/// <summary>
		/// Register object as it Type and all interfaces
		/// </summary>
		/// <typeparam name="TImpl">Object type</typeparam>
		/// <param name="instance">instance of object</param>
		public void RegisterWithInterfaces<TImpl>(TImpl instance)
		{
			var type = typeof(TImpl);
			var interfaces = type.GetInterfaces();
			RegisterInstance(instance);
			foreach (var item in interfaces)
			{
				RegisterInstance(item, instance);
			}
		}

		public bool TryGetInstance(Type parameterType, out object res)
		{
			res = null;
			try
			{
				res = GetInstance(parameterType);
				return true;
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
			}
			return false;
		}

		public bool TryGetInstance<TType>(out TType res)
		{
			res = default;
			try
			{
				res = (TType) GetInstance(typeof(TType));
				return true;
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
			}
			return false;
		}

		/// <summary>
		/// Register instance of object with all interfaces
		/// </summary>
		/// <param name="instance">object instance</param>
		public void RegisterInstanceWithInterfaces(object instance)
		{
			var type = instance.GetType();
			var interfaces = type.GetInterfaces();
			RegisterInstance(type, instance);
			foreach (var item in interfaces)
			{
				RegisterInstance(item, instance);
			}
		}

		/// <summary>
		/// Register object with all iterfaces
		/// </summary>
		/// <typeparam name="TImpl">Object type</typeparam>
		public void RegisterWithInterfaces<TImpl>()
		{
			var instance = GetInstance<TImpl>();
			RegisterWithInterfaces(instance);
		}

		/// <summary>
		/// Register type and object for this type
		/// </summary>
		/// <typeparam name="TService">registered Object type</typeparam>
		/// <typeparam name="TImpl">object type</typeparam>
		public void Register<TService, TImpl>() where TImpl : TService
		{
			if (_registeredObjects.ContainsKey(typeof(TService))
				|| (_parentContext?.ContainInstance(typeof(TService)) ?? false)) // not allowed to register objects that contains in parent context
			{
				throw new TypeRegisteredException(typeof(TService));
			}

			var interfaces = typeof(TImpl).GetInterfaces().ToList();
			if (interfaces.Contains(typeof(TService)))
			{
				_ = interfaces.Remove(typeof(TService));
			}
			_registeredObjects.Add(typeof(TService), typeof(TImpl));
			foreach (var itnrface in interfaces)
			{
				if (!_registeredObjects.ContainsKey(itnrface))
				{
					_registeredObjects.Add(itnrface, typeof(TImpl));
				}
			}
		}

		internal bool ContainInstance(Type type)
		{
			return _buildedObjects.ContainsKey(type) || (_parentContext?.ContainInstance(type) ?? false);
		}

		/// <summary>
		/// Register instance of object
		/// </summary>
		/// <typeparam name="TService">Object type</typeparam>
		/// <param name="instance">Object instance</param>
		public void RegisterInstance<TService>(TService instance)
		{
			if (!_registeredObjects.ContainsKey(typeof(TService))
				&& (!_parentContext?.TryGetInstance<TService>(out _) ?? true)) // not allowed to register objects that contains in parent context
			{
				_registeredObjects.Add(typeof(TService), instance.GetType());
				_buildedObjects.Add(typeof(TService), instance);
				return;
			}

			throw new TypeRegisteredException(typeof(TService));
		}

		/// <summary>
		/// Register instance of object with type
		/// </summary>
		/// <param name="type">type of object</param>
		/// <param name="instance">object instance</param>
		public void RegisterInstance(Type type, object instance)
		{
			if (!type.IsAssignableFrom(instance.GetType()))
			{
				throw new TypeMisMatchException(type, instance.GetType());
			}

			if (!_registeredObjects.ContainsKey(type)
				&& (!_parentContext?.TryGetInstance(type, out _) ?? true)) // not allowed to register objects that contains in parent context
			{
				_registeredObjects.Add(type, instance.GetType());
				_buildedObjects.Add(type, instance);
				return;
			}

			throw new TypeRegisteredException(type);
		}

		/// <summary>
		/// Get instance of <typeparamref name="T"/> object
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <returns>instance of <typeparamref name="T"/></returns>
		public T GetInstance<T>()
		{
			var type = typeof(T);
			if (_buildedObjects.ContainsKey(type))
			{
				return (T) _buildedObjects[type];
			}
			else if (_parentContext != null) // Search in parent context
			{
				if (_parentContext.TryGetInstance<T>(out var result))
				{
					return result;
				}
				else
				{
					if (!type.IsAbstract)
					{
						return (T) CreateInstance(type);
					}
				}
			}
			else
			{
				if (!type.IsAbstract)
				{
					return (T) CreateInstance(type);
				}
			}
			throw new TypeNotRegisteredException(type);
		}

		/// <summary>
		/// Get object instance of type
		/// </summary>
		/// <param name="type">type</param>
		/// <returns>instance as Object</returns>
		internal object GetInstance(Type type)
		{
			if (_buildedObjects.ContainsKey(type))
			{
				return _buildedObjects[type];
			}
			else if (_parentContext != null && _parentContext.ContainInstance(type))
			{
				if (_parentContext.TryGetInstance(type, out object result))
				{
					return result;
				}
				else
				{
					if (!type.IsAbstract)
					{
						return CreateInstance(type);
					}
				}
			}
			else
			{
				if (_registeredObjects.ContainsKey(type))
				{
					object created = GetInstance(_registeredObjects[type]);
					_buildedObjects.Add(type, created);
					return created;
				}
				else
				{
					if (!type.IsAbstract)
					{
						return CreateInstance(type);
					}
				}
			}
			throw new TypeNotRegisteredException(type);
		}

		/// <summary>
		/// Create instance of <paramref name="implementationType"/>
		/// </summary>
		/// <param name="implementationType">creation type</param>
		/// <returns>instance of <paramref name="implementationType"/></returns>
		private object CreateInstance(Type implementationType)
		{
			var ctor = implementationType.GetConstructors().Single();
			var paramTypes = ctor.GetParameters().Select(p => p.ParameterType);
			object[] dependencies = GetInstances(paramTypes).ToArray();
			return Activator.CreateInstance(implementationType, dependencies);
		}

		/// <summary>
		/// Get list of instances by types
		/// </summary>
		/// <param name="paramTypes">param list</param>
		/// <returns>list of instances</returns>
		private IEnumerable<object> GetInstances(IEnumerable<Type> paramTypes)
		{
			List<object> objects = new List<object>();
			foreach (var item in paramTypes)
			{
				objects.Add(GetInstance(item));
			}
			return objects;
		}

		public void Dispose()
		{
			_buildedObjects.Clear();
			_registeredObjects.Clear();
		}
	}
}
