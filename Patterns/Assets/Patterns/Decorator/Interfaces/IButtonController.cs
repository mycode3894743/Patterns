using System;
using TMPro;

namespace Patterns.Decorator.Interfaces
{
	/// <summary>
	/// Base button controller interface
	/// </summary>
	public interface IButtonController
	{
		/// <summary>
		/// OnButtonClick event 
		/// </summary>
		event Action OnButtonClicked;

		/// <summary>
		/// Link to button label. Can be null
		/// </summary>
		TextMeshProUGUI Label { get; }

		/// <summary>
		/// Change interactable state
		/// </summary>
		/// <param name="interactable">is button interactable</param>
		void SetButtonInteractable(bool interactable);
	}
}
