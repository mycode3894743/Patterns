#nullable enable
using System;
using System.Runtime.CompilerServices;
using UnityEngine.ResourceManagement.AsyncOperations;

public struct AsyncOperationHandleAwaiter<TResult> : INotifyCompletion
{
    private AsyncOperationHandle<TResult> _handle;

    public bool IsCompleted => _handle.IsDone;

    public AsyncOperationHandleAwaiter(AsyncOperationHandle<TResult> handle) => _handle = handle;

    public TResult GetResult() => _handle.Result;

    public void OnCompleted(Action continuation)
    {
        var handleCopy = _handle;
        Action<AsyncOperationHandle<TResult>>? cont = null;
        cont = h =>
        {
            if (!h.Equals(handleCopy))
                return;

            h.Completed -= cont;
            continuation();
        };

        _handle.Completed += cont;
    }
}

public static class AsyncOperationHandleAwaitable
{
    public static AsyncOperationHandleAwaiter<TResult> GetAwaiter<TResult>(this AsyncOperationHandle<TResult> handle)
    {
        return new AsyncOperationHandleAwaiter<TResult>(handle);
    }
}
