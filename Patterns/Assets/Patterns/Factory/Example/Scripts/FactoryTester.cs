using Patterns.Factory.Enum;
using UnityEngine;

namespace Patterns.Factory.Example
{
	public class FactoryTester : MonoBehaviour
	{
		private async void Start()
		{
			GameObjectFactory factory = new GameObjectFactory();

			var cube = await factory.Create(GameObjectTypes.Cube, "FirstCube", Color.red, new Vector3(0, 0, 2));
			cube.Collider.enabled = false;

			var sphere = await factory.Create("Sphere", "FirstSphere", Color.green, new Vector3(2, 0, 0));
			sphere.SetColor(Color.blue);
		}
	}
}
