using System.Threading.Tasks;
using Patterns.Factory.Enum;
using UnityEngine;

namespace Patterns.Factory.Interfaces
{
	public interface IGameObjectFactory : IFactoryAsync
	{
		/// <summary>
		/// hiearchy parent object name
		/// </summary>
		string ParentObjectName { get; }

		/// <summary>
		/// Create parent object
		/// </summary>
		/// <returns>transform of parent object</returns>
		Transform GetParentObject { get; }

		/// <summary>
		/// Create IGameObject by asset name
		/// </summary>
		/// <param name="assetName">asset name</param>
		/// <param name="name">object name</param>
		/// <param name="color">object color</param>
		/// <param name="position">object position</param>
		/// <returns>IGameObject in scene</returns>
		Task<IGameObject> Create(string assetName, string name, Color color, Vector3 position);

		/// <summary>
		/// Create IGameObject by objectType
		/// </summary>
		/// <param name="type">object type</param>
		/// <param name="name">object name</param>
		/// <param name="color">object color</param>
		/// <param name="position">object position</param>
		/// <returns>IGameObject in scene</returns>
		Task<IGameObject> Create(GameObjectTypes type, string name, Color color, Vector3 position);
	}
}
