using IOC.Base.Context.Abstraction;

namespace IOC.Base.Context
{
	/// <summary>
	/// Player contect. Contains player related entities
	/// </summary>
	public class PlayerContext : MinorContext
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="gameContext">parent context</param>
		public PlayerContext(GameContext gameContext) : base(gameContext)
		{
		}

		/// <inheritdoc/>
		public override void InitContainer()
		{
		}

		/// <summary>
		/// Register all player related entities
		/// </summary>
		public override void OnContextBuilded()
		{
		}
	}
}
