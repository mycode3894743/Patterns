using System;
using Patterns.Decorator.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Patterns.Decorator
{
	/// <summary>
	/// Decorated object
	/// </summary>
	[RequireComponent(typeof(Button))]
	public class ButtonController : MonoBehaviour, IButtonController
	{
		/// <inheritdoc/>
		public event Action OnButtonClicked;

		[Tooltip("Attached button")]
		[SerializeField] private Button _button;
		[Tooltip("Attached label")]
		[SerializeField] private TextMeshProUGUI _label;

		/// <summary>
		/// Button finder
		/// </summary>
		protected Button Button
		{
			get
			{
				if (_button == null)
				{
					_button = GetComponent<Button>();
				}
				return _button;
			}
		}

		/// <summary>
		/// Label finder
		/// </summary>
		public TextMeshProUGUI Label
		{
			get
			{
				if (_label == null)
				{
					_label = GetComponentInChildren<TextMeshProUGUI>();
				}

				return _label;
			}
		}

		///<inheritdoc/>
		public void SetButtonInteractable()
		{
			OnButtonClicked?.Invoke();
		}

		private void OnEnable()
		{
			//Subscribe on button click
			Button.onClick.AddListener(SetButtonInteractable);
		}

		private void OnDisable()
		{
			//Unsubscribe from button click
			Button.onClick.RemoveListener(SetButtonInteractable);
		}

		/// <inheritdoc/>
		public virtual void SetButtonInteractable(bool interactable)
		{
			Button.interactable = interactable;
		}
	}
}
