using System;

public class FirebaseAnalyticsAdapter : ILoginAnalyticsService
{
	private readonly IFirebaseAnalytic _firebaseAnalytic;

	public FirebaseAnalyticsAdapter(IFirebaseAnalytic firebaseAnalytic)
	{
		_firebaseAnalytic = firebaseAnalytic;
	}

	public void SendLoginEvent(DateTime loginDate, string userId)
	{
		_firebaseAnalytic.LogEvent("login_event", new Parameter[2]
		{
			new Parameter("login_date", loginDate.ToString("dd.MM HH/:mm/:ss")),
			new Parameter("user_id", userId)
		});
	}
}
