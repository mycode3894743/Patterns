﻿#nullable enable
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public struct AsyncOperationAwaiter : INotifyCompletion
{
    private AsyncOperation _asyncOperation;

    public bool IsCompleted => _asyncOperation.isDone;

    public AsyncOperationAwaiter(AsyncOperation asyncOperation) => _asyncOperation = asyncOperation;

    public void GetResult() { }

    public void OnCompleted(Action continuation)
    {
        Action<AsyncOperation>? cont = null;
        cont = h =>
        {
            h.completed -= cont;
            continuation();
        };

        _asyncOperation.completed += cont;
    }
}

public static class AsyncOperationAwaitable
{
    public static AsyncOperationAwaiter GetAwaiter(this AsyncOperation operation)
    {
        return new AsyncOperationAwaiter(operation);
    }
}
