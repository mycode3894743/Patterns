using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArmyGroup : IArmyUnit
{
	[SerializeField] private List<IArmyUnit> _units = new List<IArmyUnit>();

	public float GetDamage()
	{
		float totalDamage = 0;
		foreach (var unit in _units)
		{
			totalDamage += unit.GetDamage();
		}
		return totalDamage;
	}

	public float GetSpeed()
	{
		//Get minimum speed to get speed of whole group
		return _units.Min(x => x.GetSpeed());
	}

	public int GetTotalCount()
	{
		int totalCount = 0;
		foreach (var unit in _units)
		{
			totalCount += unit.GetTotalCount();
		}
		return totalCount;
	}

	public void MoveToCoord(Vector3 cord)
	{
		foreach (var unit in _units)
		{
			unit.MoveToCoord(cord);
		}
	}

	public void AddUnits(IArmyUnit unit)
	{
		_units.Add(unit);
	}

	public void RemoveUnits(IArmyUnit unit)
	{
		if (_units.Contains(unit))
		{
			_units.Remove(unit);
		}
	}
}
