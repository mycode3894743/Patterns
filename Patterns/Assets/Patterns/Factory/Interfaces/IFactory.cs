using System;
using System.Threading.Tasks;

namespace Patterns.Factory.Interfaces
{
	/// <summary>
	/// Factory base
	/// </summary>
	public interface IFactory
	{
		/// <summary>
		/// Type of object
		/// </summary>
		Type InstanceType { get; }

		/// <summary>
		/// Asset path
		/// </summary>
		string AssetPath { get; }
	}

	/// <summary>
	/// Async factory
	/// </summary>
	public interface IFactoryAsync : IFactory
	{
		/// <summary>
		/// Get instance Async
		/// </summary>
		/// <param name="data">initial data</param>
		/// <returns>result</returns>
		Task<object> GetInstanceAsync(object data);
	}

	/// <summary>
	/// Async generic factory
	/// </summary>
	/// <typeparam name="T">creation type</typeparam>
	/// <typeparam name="TData">creation type data</typeparam>
	public interface IFactoryAsync<T, TData> : IFactory where T : class where TData : class
	{
		Task<T> GetInstanceAsync(TData data);
	}

	/// <summary>
	/// Sync factory
	/// </summary>
	public interface IFactorySync : IFactory
	{
		/// <summary>
		/// Get instance
		/// </summary>
		/// <param name="data">initial data</param>
		/// <returns>instance</returns>
		object GetInstance(object data);
	}
}
