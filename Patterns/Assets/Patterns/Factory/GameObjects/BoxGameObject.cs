using Patterns.Factory.Interfaces;
using UnityEngine;

namespace Patterns.Factory.GameObjects
{
	[RequireComponent(typeof(BoxCollider))]
	[RequireComponent(typeof(MeshRenderer))]
	public class BoxGameObject : MonoBehaviour, IGameObject
	{
		[SerializeField] private BoxCollider _collider;
		[SerializeField] private MeshRenderer _meshRenderer;

		public Transform Transform => transform;

		public Collider Collider
		{
			get
			{
				if (_collider == null)
				{
					_collider = GetComponent<BoxCollider>();
				}
				return _collider;
			}
		}

		private MeshRenderer MeshRenderer
		{
			get
			{
				if (_meshRenderer == null)
				{
					_meshRenderer = GetComponent<MeshRenderer>();
				}
				return _meshRenderer;
			}
		}

		public void SetColor(Color color)
		{
			MeshRenderer.material.color = color;
		}
	}
}
