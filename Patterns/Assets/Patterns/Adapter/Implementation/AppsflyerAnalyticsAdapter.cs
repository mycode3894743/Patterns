using System;
using System.Collections.Generic;

public class AppsflyerAnalyticsAdapter : ILoginAnalyticsService
{
	private readonly IAppsflyerAnalytics _appsflyerAnalytics;

	public AppsflyerAnalyticsAdapter(IAppsflyerAnalytics appsflyerAnalytics)
	{
		_appsflyerAnalytics = appsflyerAnalytics;
	}

	public void SendLoginEvent(DateTime loginDate, string userId)
	{
		_appsflyerAnalytics.SendEvent("g_login", new Dictionary<string, object>(2)
		{
			{"f_login_date", loginDate},
			{"f_user_id", userId}
		});
	}
}
