using Patterns.Decorator.Interfaces;

namespace Patterns.Decorator
{
	/// <summary>
	/// Label text changer decorator
	/// </summary>
	public class ButtonControllerLabelDecorator : ButtonControllerDecorator
	{
		/// <summary>
		/// New label text
		/// </summary>
		private readonly string _newLabelText;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="controller">wrapper</param>
		/// <param name="newLabelText">new label text</param>
		public ButtonControllerLabelDecorator(IButtonController controller, string newLabelText) : base(controller)
		{
			_newLabelText = newLabelText;
		}

		///<inheritdoc/>
		public override void SetButtonInteractable(bool interactable)
		{
			if (Label != null)
			{
				Label.text = _newLabelText;
			}
			base.SetButtonInteractable(interactable);
		}
	}
}
