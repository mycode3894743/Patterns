﻿using System;
using UnityEngine;

public class AddressableReferenceHolder : MonoBehaviour
{
    public event Action onDestroyAction;

    public virtual void OnDestroy()
    {
        ReleaseOwnedAssets();
    }

    protected void ReleaseOwnedAssets()
    {
        onDestroyAction?.Invoke();
        onDestroyAction = null;
    }
}
