using System.Collections.Generic;

public interface IAppsflyerAnalytics 
{
	void SendEvent(string eventName, Dictionary<string, object> eventValues);
}
