using Patterns.Decorator.Interfaces;
using UnityEngine;

namespace Patterns.Decorator
{
	/// <summary>
	/// Logger decorator, print log to console
	/// </summary>
	public class ButtonControllerLoggerDecorator : ButtonControllerDecorator
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="buttonController">wrapper</param>
		public ButtonControllerLoggerDecorator(IButtonController buttonController) : base(buttonController)
		{

		}

		/// <inheritdoc/>
		public override void SetButtonInteractable(bool interactable)
		{
			Debug.Log($"Button set interactable to {interactable}");
			base.SetButtonInteractable(interactable);
		}
	}
}
