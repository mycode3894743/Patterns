using UnityEngine;

namespace Patterns.Factory.Interfaces
{
	public interface IGameObject
	{
		Transform Transform { get; }
		Collider Collider { get; }

		void SetColor(Color color);
	}
}
