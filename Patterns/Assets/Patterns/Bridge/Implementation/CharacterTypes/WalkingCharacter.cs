using UnityEngine;

public class WalkingCharacter : ICharacter
{
	[SerializeField] private string _name;

	private IProfession _profession;

	public string Name => _name;

	public IProfession Profession => _profession;

	public void TryInteract()
	{
		//Do some stuff
		_profession.OpenDialog();
	}

	public void RunUsualBehavior()
	{
		// Run walk animation, create path etc.
	}

}
