using System.Collections.Generic;
using System.Linq;
using IOC.Base.Context;
using UnityEngine;

namespace IOC.Utils
{
	/// <summary>
	/// Game context creator
	/// </summary>
	[DefaultExecutionOrder(int.MinValue)]
	public class ContextCollector : MonoBehaviour
	{
		/// <summary>
		/// MonoBehaviours to register in game context
		/// </summary>
		[SerializeField] private List<MonoBehaviour> _monoBehavioursInstances = new();

		[SerializeField] private List<ScriptableObject> _configs = new();

		[SerializeField] private List<Installer> _installers = new();

		/// <summary>
		/// Game Context instance
		/// </summary>
		private GameContext _gameContext = default;

		public void Awake()
		{
			_gameContext = GameContext.GetGameContext();
			_gameContext.RegisteInstances(_configs.Select(x => (object) x));
			_gameContext.RegisteInstances(_monoBehavioursInstances.Select(x => (object) x));
			_gameContext.RegisterInstaller(_installers);

			AfterRegister();

			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			Input.multiTouchEnabled = false;
			Application.targetFrameRate = 60;
		}

		private void AfterRegister()
		{

			foreach (var instance in _monoBehavioursInstances)
			{
				var type = instance.GetType();
				var method = type.GetMethod("Inject");
				if (method != null)
				{
					var parameters = method.GetParameters();
					var objects = new List<object>(parameters.Length);
					foreach (var param in parameters)
					{
						if (!_gameContext.ContainInstance(param.ParameterType) || !_gameContext.TryGetInstance(param.ParameterType, out object res))
						{
							throw new KeyNotFoundException($"instance of {param.ParameterType} not found for type {type}");
						}
						else
						{
							objects.Add(res);
						}
					}
					_ = method.Invoke(instance, objects.ToArray());
				}
			}
		}
	}
}
