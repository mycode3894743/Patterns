using Patterns.Decorator.Interfaces;
using UnityEngine;

namespace Patterns.Decorator
{
	/// <summary>
	/// Sound player decorator, play clip on change state
	/// </summary>
	public class ButtonControllerSoundDecorator : ButtonControllerDecorator
	{
		/// <summary>
		/// Sound source
		/// </summary>
		private readonly AudioSource _audioSource;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="buttonController">wrapper</param>
		/// <param name="audioSource">sound source</param>
		public ButtonControllerSoundDecorator(IButtonController buttonController, AudioSource audioSource) : base(buttonController)
		{
			_audioSource = audioSource;
		}

		/// <inheritdoc/>
		public override void SetButtonInteractable(bool interactable)
		{
			_audioSource?.PlayOneShot(_audioSource.clip);
			base.SetButtonInteractable(interactable);
		}
	}
}
