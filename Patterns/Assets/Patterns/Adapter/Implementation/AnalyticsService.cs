using System;
using System.Collections.Generic;
using UnityEngine;

public class AnalyticsService : MonoBehaviour
{
	[SerializeField] private bool _useFirebaseAnalytics;
	[SerializeField] private bool _useAppsflyerAnalytics;

	private List<ILoginAnalyticsService> _loginAnalyticsServiceList = new(2);

	public void Init()
	{
		if(_useAppsflyerAnalytics)
		{
			// Init adapter (null cause there is no service)
			_loginAnalyticsServiceList.Add(new AppsflyerAnalyticsAdapter(null));
		}
		if(_useFirebaseAnalytics)
		{
			//Init adapter (null cause there is no service)
			_loginAnalyticsServiceList.Add(new FirebaseAnalyticsAdapter(null));
		}
	}

	public void SendLogin()
	{
		foreach(var analyticsService in _loginAnalyticsServiceList)
		{
			analyticsService.SendLoginEvent(DateTime.Now, "0000_02facb4");
		}
	}
}
