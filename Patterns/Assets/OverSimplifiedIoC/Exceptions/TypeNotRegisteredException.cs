using System;

public class TypeNotRegisteredException : Exception
{
	public TypeNotRegisteredException(Type type) : base($"{type} not registered!")
	{

	}
}
