using System;
using System.Threading.Tasks;
using Patterns.Factory.Enum;
using Patterns.Factory.Interfaces;
using UnityEngine;

namespace Patterns.Factory
{
	public class GameObjectFactory : IGameObjectFactory
	{
		///<inheritdoc/>
		public string ParentObjectName { get; }

		/// <summary>
		/// Parent object
		/// </summary>
		protected Transform _parentObject;

		/// <inheritdoc/>
		public Transform GetParentObject
		{
			get
			{
				if (_parentObject == null)
				{
					GameObject parent = new GameObject(ParentObjectName);
					_parentObject = parent.transform;
				}
				return _parentObject;
			}
		}

		/// <inheritdoc/>
		public Type InstanceType => typeof(IGameObject);

		/// <inheritdoc/>
		public string AssetPath => "Factory";

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parentObjectsName">parent game object name</param>
		public GameObjectFactory(string parentObjectsName = "IGameObjects")
		{
			ParentObjectName = parentObjectsName;
		}

		/// <inheritdoc/>
		public Task<IGameObject> Create(GameObjectTypes type, string name, Color mapItemData, Vector3 position)
		{
			return Create(type.ToString("f"), name, mapItemData, position);
		}

		///<inheritdoc/>
		public async Task<IGameObject> Create(string assetName, string name, Color mapItemData, Vector3 position)
		{
			string path = $"{AssetPath}/{assetName}";
			bool isObjectPrefabExist = await AddressablesManager.ValidateAdressAsync(path);
			if (isObjectPrefabExist)
			{
				GameObject gameObject = await AddressablesManager.InstantiateGo<GameObject>(path);

				//rename
				gameObject.name = $"{name}";
				gameObject.SetActive(false);
				IGameObject viewSetup = gameObject.GetComponent<IGameObject>();
				if (viewSetup == null)
				{
					return null;
				}

				viewSetup.Transform.SetParent(GetParentObject);
				viewSetup.Transform.position = position;
				viewSetup.SetColor(mapItemData);

				gameObject.SetActive(true);
				return viewSetup;
			}
			Debug.LogError($"Prefab {path} doesn't exist");
			return default;
		}

		///<inheritdoc/>
		[Obsolete]
		public async Task<object> GetInstanceAsync(object data)
		{
			var result = await Create("Cube", "Box", Color.white, Vector3.zero);
			return result;
		}
	}
}
