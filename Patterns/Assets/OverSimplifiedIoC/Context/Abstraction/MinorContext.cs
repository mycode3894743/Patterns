
namespace IOC.Base.Context.Abstraction
{
	/// <summary>
	/// Context that has parent context
	/// </summary>
	public abstract class MinorContext : BaseContext
	{
		/// <summary>
		/// Parent context
		/// </summary>
		protected BaseContext _parentContext;

		/// <summary>
		/// Constructor with parent
		/// </summary>
		/// <param name="parent">parent context</param>
		public MinorContext(BaseContext parent) : base()
		{
			_parentContext = parent;
			_currentContainer.AddParentContext(_parentContext);
		}

		/// <summary>
		/// Here you can register all entities
		/// </summary>
		public virtual void InitContainer()
		{
			BuildContainer();
		}

		private void BuildContainer()
		{
			_currentContainer.BuildRegistered();
			OnContextBuilded();
		}

		public abstract void OnContextBuilded();
	}
}
