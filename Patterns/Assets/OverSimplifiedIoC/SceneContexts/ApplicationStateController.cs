using System;
using IOC.Base.Context;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ApplicationStateController : MonoBehaviour
{
	private GameContext _gameContext;

	public void Start()
	{
		_gameContext = GameContext.GetGameContext();
		DontDestroyOnLoad(gameObject);
#if UNITY_EDITOR
		EditorApplication.playModeStateChanged += OnPlayModeChange;
	}

	private void OnPlayModeChange(PlayModeStateChange state)
	{
		if (state == PlayModeStateChange.ExitingPlayMode)
		{
			OnApplicationQuit();
		}
#endif
	}

	private void OnApplicationQuit()
	{
		_gameContext.Dispose();
		GC.SuppressFinalize(_gameContext);
	}
}
