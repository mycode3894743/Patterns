using System;
using System.Runtime.InteropServices;

public sealed class Parameter : IDisposable
{
	private HandleRef swigCPtr;

	private bool swigCMemOwn;

	internal Parameter(IntPtr cPtr, bool cMemoryOwn)
	{
		swigCMemOwn = cMemoryOwn;
		swigCPtr = new HandleRef(this, cPtr);
	}

	internal static HandleRef getCPtr(Parameter obj)
	{
		return obj?.swigCPtr ?? new HandleRef(null, IntPtr.Zero);
	}

	internal static HandleRef swigRelease(Parameter obj)
	{
		if (obj != null)
		{
			if (!obj.swigCMemOwn)
			{
				throw new ApplicationException("Cannot release ownership as memory is not owned");
			}

			HandleRef result = obj.swigCPtr;
			obj.swigCMemOwn = false;
			obj.Dispose();
			return result;
		}

		return new HandleRef(null, IntPtr.Zero);
	}

	~Parameter()
	{
		Dispose(disposing: false);
	}

	public void Dispose()
	{
		Dispose(disposing: true);
		GC.SuppressFinalize(this);
	}

	public void Dispose(bool disposing)
	{
			GC.SuppressFinalize(this);
	}

	public Parameter(string parameterName, string parameterValue)
	{
	}

	public Parameter(string parameterName, long parameterValue)
	{
	}

	public Parameter(string parameterName, double parameterValue)
	{
	}
}