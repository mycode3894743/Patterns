public interface IFirebaseAnalytic
{
	void LogEvent(string name, params Parameter[] parameters);
}
