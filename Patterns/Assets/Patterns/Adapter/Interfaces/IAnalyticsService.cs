using System;

public interface ILoginAnalyticsService
{
	void SendLoginEvent(DateTime loginDate, string userId);
}
