using UnityEngine;

namespace Patterns.Decorator.Example
{
	/// <summary>
	/// Using example class
	/// </summary>
	public class UIController : MonoBehaviour
	{
		[Tooltip("First button")]
		[SerializeField] private ButtonController _testButton;
		[Tooltip("Second button")]
		[SerializeField] private ButtonController _secondTestButton;
		[Space]
		[Tooltip("Click sound source")]
		[SerializeField] private AudioSource _audioSource;

		public void Awake()
		{
			//Subscribe on click events
			_testButton.OnButtonClicked += OnTestButtonClick;
			_secondTestButton.OnButtonClicked += OnSecondTestClick;
		}

		/// <summary>
		/// Decorate set interaction method
		/// </summary>
		private void OnSecondTestClick()
		{
			ButtonControllerLoggerDecorator loggerDecorator = new ButtonControllerLoggerDecorator(_secondTestButton);
			ButtonControllerSoundDecorator soundDecorator = new ButtonControllerSoundDecorator(loggerDecorator, _audioSource);
			ButtonControllerLabelDecorator labelDecorator = new ButtonControllerLabelDecorator(soundDecorator, "Oh yes! tnx!");
			labelDecorator.SetButtonInteractable(false);
			loggerDecorator = new ButtonControllerLoggerDecorator(_testButton);
			labelDecorator = new ButtonControllerLabelDecorator(loggerDecorator, "Hey! click me now");
			labelDecorator.SetButtonInteractable(true);
		}

		/// <summary>
		/// Decorate set interaction method
		/// </summary>
		private void OnTestButtonClick()
		{
			ButtonControllerLoggerDecorator loggerDecorator = new ButtonControllerLoggerDecorator(_testButton);
			ButtonControllerSoundDecorator soundDecorator = new ButtonControllerSoundDecorator(loggerDecorator, _audioSource);
			ButtonControllerLabelDecorator labelDecorator = new ButtonControllerLabelDecorator(soundDecorator, "Finaly! tnx");
			labelDecorator.SetButtonInteractable(false);
			loggerDecorator = new ButtonControllerLoggerDecorator(_secondTestButton);
			labelDecorator = new ButtonControllerLabelDecorator(loggerDecorator, "What about me?!");
			labelDecorator.SetButtonInteractable(true);
		}

		private void OnDestroy()
		{
			//Unsubscribe from click events
			_testButton.OnButtonClicked -= OnTestButtonClick;
			_secondTestButton.OnButtonClicked -= OnSecondTestClick;
		}
	}
}
