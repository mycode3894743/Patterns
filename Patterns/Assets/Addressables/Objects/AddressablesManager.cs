﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public static class AddressablesManager
{
    public static string EnvironmentStr;
    public static string AppVersion;

    static AddressablesManager()
    {
        EnvironmentStr = "Dev";
        AppVersion = $"{"0.8.1"}.{"1050"}";
        Debug.LogWarning($"App Version: {AppVersion}");

    }

    /// <summary>
    /// Load asset by name. Reference handler release will be attached to OnDestroy of Holder object.
    /// </summary>
    /// <param name="assetNameOrLabel">Asset name or label</param>
    /// <param name="holder">Holder object, on destruction loaded handle will be released</param>
    /// <param name="onComplete">Callback with loaded object</param>
    /// <returns></returns>
    public static AsyncOperationHandle<T> GetAsset<T>(string assetNameOrLabel, AddressableReferenceHolder holder, Action<T> onComplete = null) where T : Object
    {
        var handler = Addressables.LoadAssetAsync<T>(assetNameOrLabel);
        handler.Completed += handel => onComplete?.Invoke(handel.Result);
        holder.onDestroyAction += () => Addressables.Release(handler);
        return handler;
    }

    public static AsyncOperationHandle<T> GetAsset<T>(string assetNameOrLabel) where T : Object
    {
        return Addressables.LoadAssetAsync<T>(assetNameOrLabel);
    }

    public static async Task<AsyncOperationHandle<T>> GetValidAssetAsync<T>(string assetNameOrLabel, string defaultAssetName) where T : Object
    {
        bool isValid = await ValidateAdressAsync(assetNameOrLabel);
        if (isValid)
        {
            AsyncOperationHandle<T> task = Addressables.LoadAssetAsync<T>(assetNameOrLabel);
            while (!task.IsDone)
            {
                await Task.Delay(100);
            }

            return task;
        }
        else
        {
            AsyncOperationHandle<T> task = Addressables.LoadAssetAsync<T>(defaultAssetName);
            while (!task.IsDone)
            {
                await Task.Delay(100);
            }

            return task;
        }
    }

    /// <summary>
    /// Load asset with default fallback option if original name is not found.
    /// </summary>
    /// <param name="assetNameOrLabel">Asset name or label</param>
    /// <param name="fallbackNameOrLabel">Default fallback asset name or label</param>
    /// <param name="holder">Owner of the asset reference to dispose of later</param>
    /// <param name="onComplete">Will be called on whatever asset is loaded</param>
    /// <param name="onDefault">Will be called only if default asset was loaded, executes AFTER onComplete</param>
    /// <returns> No return value. If handler is required - use direct methods. </returns>
    public static void GetAssetWithFallback<T>(string assetNameOrLabel, string fallbackNameOrLabel, AddressableReferenceHolder holder, Action<T> onComplete = null, Action<T> onDefault = null) where T : Object
    {
        ValidateAdressAsync(assetNameOrLabel,
            () => GetAsset<T>(assetNameOrLabel, holder, onComplete),
            () => GetAsset<T>(fallbackNameOrLabel, holder, (obj) =>
                {
# if UNITY_EDITOR
                    Debug.LogWarning("Missing asset with name: " + assetNameOrLabel);
#endif
                    onComplete?.Invoke(obj);
                    onDefault?.Invoke(obj);
                }
            ));
    }

    /// <summary>
    /// Validates if asset with a name exists in asset library. Warning - this is async operation and will not return a value!
    /// </summary>
    /// <param name="assetNameOrLabel">Asset name or label</param>
    /// <param name="onComplete">Callback -> asset exists in database</param>
    /// <param name="onFail">Callback -> asset was not found in database</param>
    public static void ValidateAdressAsync(string assetNameOrLabel, Action onComplete = null, Action onFail = null)
    {
        AsyncOperationHandle<IList<IResourceLocation>> handle = Addressables.LoadResourceLocationsAsync(assetNameOrLabel);
        handle.CompletedTypeless += handler =>
        {
            if (handle.Result.Count > 0)
                onComplete?.Invoke();
            else
                onFail?.Invoke();
        };
    }

    public static async Task<bool> ValidateAdressAsync(string assetNameOrLabel)
    {
        var handle = await Addressables.LoadResourceLocationsAsync(assetNameOrLabel);
        return handle.Count > 0;
    }

    /// <summary>
    /// Should be used only with static classes. Class is responsible for releasing asset handle when it is no longer needed!
    /// </summary>
    /// <param name="assetNameOrLabel">Asset name or label</param>
    /// <param name="onComplete"> Callback -> asset successfully loaded </param>
    /// <returns></returns>
    public static AsyncOperationHandle<T> GetAssetUnmanaged<T>(string assetNameOrLabel, Action<T> onComplete = null) where T : Object
    {
        var handler = Addressables.LoadAssetAsync<T>(assetNameOrLabel);
        handler.Completed += handel => onComplete?.Invoke(handel.Result);
        return handler;
    }

    //TODO: Add fallback options for prefabs? Do we need them?
    #region Instantiate Game object
    public static AsyncOperationHandle<GameObject> InstantiateGo<T>(string assetNameOrLabel, Action<T> onComplete = null, Transform parent = null)
        where T : Object
    {
        return InstantiateGo<T>(assetNameOrLabel, Vector3.zero, Quaternion.identity, parent, onComplete);
    }

    public static AsyncOperationHandle<GameObject> InstantiateGo<T>(string assetNameOrLabel,
        Vector3 position, Quaternion rotation, Transform parent = null,
        Action<T> onComplete = null)
        where T : Object
    {
        var handler = Addressables.InstantiateAsync(assetNameOrLabel, position, rotation, parent);
        handler.Completed += handle =>
        {
            onComplete?.Invoke(handler.Result as T);

            handler.Result.AddComponent<AddressableReferenceHolder>()
                .onDestroyAction += () => Addressables.Release(handler);
        };

        return handler;
    }
    #endregion

    #region Scene Load

    private static SceneInstance? _activeScene = null;
    public static AsyncOperationHandle<SceneInstance> LoadSceneAsync(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Single)
    {
        var handler = Addressables.LoadSceneAsync(sceneName, loadMode);
        handler.Completed += handle => { _activeScene = handler.Result; };
        return handler;
    }

    public static AsyncOperationHandle<SceneInstance> UnloadSceneAsync(SceneInstance scene)
    {
        return Addressables.UnloadSceneAsync(scene);
    }

    public static SceneInstance? GetActiveScene()
    {
        return _activeScene;
    }
    #endregion

    #region Download assets

    public static AsyncOperationHandle DownloadAssets(string label)
    {
        return Addressables.DownloadDependenciesAsync(label);
    }

    public static AsyncOperationHandle<long> GetDownloadSize(string label)
    {
        return Addressables.GetDownloadSizeAsync(label);
    }

    #endregion
}
