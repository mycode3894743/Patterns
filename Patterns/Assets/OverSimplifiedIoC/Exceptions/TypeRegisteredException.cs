using System;

public class TypeRegisteredException : Exception
{
	public TypeRegisteredException(Type type) : base($"{type} already registered!")
	{

	}
}
