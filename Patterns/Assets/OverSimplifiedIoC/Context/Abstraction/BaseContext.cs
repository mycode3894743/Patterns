using System;
using System.Collections.Generic;
using System.Linq;
using IOC.Base.Containers;
using IOC.Utils;
using UnityEngine;

namespace IOC.Base.Context.Abstraction
{
	/// <summary>
	/// Base context
	/// </summary>
	public abstract class BaseContext : IDisposable
	{
		/// <summary>
		/// Destroy event
		/// </summary>
		public virtual event Action<BaseContext> OnDestroy = delegate { };
		/// <summary>
		/// Current context container
		/// </summary>
		protected readonly Container _currentContainer;
		/// <summary>
		/// Included context, current context can hold child containers
		/// </summary>
		protected readonly List<BaseContext> _includedContexts = new();

		/// <summary>
		/// Constructor
		/// </summary>
		protected BaseContext()
		{
			_currentContainer = new Container();
			_includedContexts = new();
		}

		/// <summary>
		/// Constructor with included contexts
		/// </summary>
		/// <param name="included">included contexts</param>
		protected BaseContext(params BaseContext[] included)
		{
			_currentContainer = new Container();
			_includedContexts = included.ToList();
		}

		/// <summary>
		/// Add context as child
		/// </summary>
		/// <param name="context">added context</param>
		protected virtual void AddContext(BaseContext context)
		{
			context.OnDestroy += RemoveContext;
			_includedContexts.Add(context);
		}

		/// <summary>
		/// Remove context from child
		/// </summary>
		/// <param name="context">context to remove</param>
		protected virtual void RemoveContext(BaseContext context)
		{
			if (_includedContexts.Contains(context))
			{
				Debug.Log($"Context unregister {context.GetType().Name}");
				_ = _includedContexts.Remove(context);
				return;
			}

			Debug.LogWarning("Context wasn't registered in parent, probably it was created manualy");
		}

		/// <summary>
		/// Register list of instances, can use after initialization
		/// </summary>
		/// <param name="instances">List of instances</param>
		public virtual void RegisteInstances(IEnumerable<object> instances)
		{
			foreach (object item in instances)
			{
				_currentContainer.RegisterInstanceWithInterfaces(item);
			}
		}

		/// <summary>
		/// Register Items
		/// </summary>
		/// <typeparam name="TService"></typeparam>
		/// <typeparam name="TImpl"></typeparam>
		internal virtual void RegisterItem<TService, TImpl>() where TImpl : TService
		{
			_currentContainer.Register<TService, TImpl>();
		}

		public virtual void RegisterInstaller(IEnumerable<Installer> installers)
		{
			foreach (var item in installers)
			{
				item.Install(this);
			}
			_currentContainer.BuildRegistered();
		}

		/// <summary>
		/// Try get instance of <typeparamref name="T"/> from context and parent contexts
		/// </summary>
		/// <typeparam name="T">Type of object</typeparam>
		/// <param name="result">resulted objects</param>
		/// <returns>is get success</returns>
		public virtual bool TryGetInstance<T>(out T result)
		{
			result = default;
			try
			{
				result = _currentContainer.GetInstance<T>();
			}
			catch (Exception ex)
			{
				Debug.LogException(ex);
			}
			return result != null;
		}

		public virtual bool ContainInstance(Type type)
		{
			return _currentContainer.ContainInstance(type);
		}

		/// <summary>
		/// Try get instance of type from context and parent contexts
		/// </summary>
		/// <param name="type">type of objects</param>
		/// <param name="result">instance of object</param>
		/// <returns>is successed</returns>
		public virtual bool TryGetInstance(Type type, out object result)
		{
			result = default;
			object instance = _currentContainer.GetInstance(type);

			if (instance != null)
			{
				result = instance;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Dispose object
		/// </summary>
		public virtual void Dispose()
		{
			OnDestroy?.Invoke(this);
			for (int i = 0; i < _includedContexts.Count; i += 0)
			{
				if (_includedContexts.Count > i)
				{
					_includedContexts[i].Dispose();
				}
				if (_includedContexts.Count > i)
				{
					_includedContexts.RemoveAt(i);
				}
			}
		}
	}
}
