public interface ICharacter 
{
	string Name { get; }
	IProfession Profession { get; }
	void TryInteract();

	void RunUsualBehavior();
}
