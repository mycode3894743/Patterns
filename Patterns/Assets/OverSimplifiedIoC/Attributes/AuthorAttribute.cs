﻿[System.AttributeUsage(System.AttributeTargets.Class |
					   System.AttributeTargets.Struct)
]
public class AuthorAttribute : System.Attribute
{
	private readonly string Name;
	public double Version;

	public AuthorAttribute(string name)
	{
		Name = name;
		Version = 1.0;
	}
}