using System;
using Patterns.Decorator.Interfaces;
using TMPro;

namespace Patterns.Decorator
{
	/// <summary>
	/// Base decorator
	/// </summary>
	public class ButtonControllerDecorator : IButtonController
	{
		/// <inheritdoc/>
		public event Action OnButtonClicked;

		/// <summary>
		/// Decorate object
		/// </summary>
		protected IButtonController _buttonController;

		/// <summary>
		/// Label link
		/// </summary>
		public TextMeshProUGUI Label => _buttonController.Label;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="buttonController">wraped object</param>
		public ButtonControllerDecorator(IButtonController buttonController)
		{
			_buttonController = buttonController;
		}

		///<inheritdoc/>
		public virtual void SetButtonInteractable(bool interactable)
		{
			_buttonController.SetButtonInteractable(interactable);
		}
	}
}
