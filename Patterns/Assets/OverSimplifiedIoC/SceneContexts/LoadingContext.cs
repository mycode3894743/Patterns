using IOC.Base.Context;
using IOC.Base.Context.Abstraction;
using UnityEngine.SceneManagement;

namespace IOC.Examples
{
	/// <summary>
	/// Loading scene context
	/// </summary>
	public class LoadingContext : SceneContext
	{

		public LoadingContext(Scene scene, BaseContext parent) : base(scene, parent)
		{
			UnityEngine.Debug.Log("Loading scene contex registered");
		}

		public override void InitContainer()
		{
			// Get instance from parent context
			if (_parentContext.TryGetInstance<Example>(out _))
			{
				UnityEngine.Debug.Log("Resolve Example");
			}

			// Register object in current scene context
			_currentContainer.Register<ObjectExample, ObjectExample>();

			base.InitContainer();
		}

		public override void OnContextBuilded()
		{

			//Get instance of registered object
			var exampleObject = _currentContainer.GetInstance<ObjectExample>();

			//Run code from resolved object
			exampleObject.DoSomeStuff();
		}
	}
}
