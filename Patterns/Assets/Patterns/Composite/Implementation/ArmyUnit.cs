using UnityEngine;

public class ArmyUnit : IArmyUnit
{
	[SerializeField] private float _damage;
	[SerializeField] private float _speed;
	[SerializeField] private int _count;

	public float GetDamage() => _damage;

	public float GetSpeed() => _speed;

	public int GetTotalCount() => _count;

	public void MoveToCoord(Vector3 cord)
	{
		Debug.Log($"move to cord {cord}");
	}
}
