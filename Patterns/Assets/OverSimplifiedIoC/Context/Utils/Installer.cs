using IOC.Base.Context.Abstraction;

namespace IOC.Utils
{
	public abstract class Installer : UnityEngine.ScriptableObject
	{
		public abstract void Install(BaseContext baseContext);
	}
}
