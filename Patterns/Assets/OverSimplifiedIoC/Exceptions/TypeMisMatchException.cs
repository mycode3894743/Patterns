using System;

public class TypeMisMatchException : Exception
{
	public TypeMisMatchException(Type type1, Type type2) : base($"{type2} Is not assignable from {type1}")
	{
	}
}
