using UnityEngine;

public class ObjectExample
{
	private readonly IExample _example;

	public ObjectExample(IExample example)
	{
		_example = example;
	}

	public void DoSomeStuff()
	{
		Debug.Log("DoSomeStuff");
		_example.DoStuff();
	}
}
