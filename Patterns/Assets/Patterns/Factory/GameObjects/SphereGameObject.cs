using Patterns.Factory.Interfaces;
using UnityEngine;

namespace Patterns.Factory.GameObjects
{
	[RequireComponent(typeof(SphereCollider))]
	[RequireComponent(typeof(MeshRenderer))]
	public class SphereGameObject : MonoBehaviour, IGameObject
	{
		[SerializeField] private SphereCollider _collider;
		[SerializeField] private MeshRenderer _meshRenderer;

		public Transform Transform => transform;

		public Collider Collider
		{
			get
			{
				if (_collider == null)
				{
					_collider = GetComponent<SphereCollider>();
				}
				return _collider;
			}
		}

		private MeshRenderer MeshRenderer
		{
			get
			{
				if (_meshRenderer == null)
				{
					_meshRenderer = GetComponent<MeshRenderer>();
				}
				return _meshRenderer;
			}
		}

		public void SetColor(Color color)
		{
			MeshRenderer.material.color = color;
		}
	}
}
