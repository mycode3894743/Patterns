using UnityEngine;

public interface IArmyUnit 
{
	int GetTotalCount();
	float GetDamage();
	float GetSpeed();
	void MoveToCoord(Vector3 cord);
}
