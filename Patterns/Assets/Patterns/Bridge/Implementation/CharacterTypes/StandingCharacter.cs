using UnityEngine;

public class StandingCharacter : ICharacter
{
	[SerializeField] private string _name;

	private IProfession _profession;

	public string Name => _name;

	public IProfession Profession => _profession;

	public void RunUsualBehavior()
	{
		//Run animation sequence
		//Make some noise
		//etc.
	}

	public void TryInteract()
	{
		_profession.OpenDialog();
	}
}
