using System;
using System.Linq;
using IOC.Base.Context.Abstraction;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace IOC.Base.Context
{
	/// <summary>
	/// Main game context, can controll all child contexts, create once by time
	/// </summary>
	public sealed class GameContext : BaseContext, IDisposable
	{
		/// <summary>
		/// instance of game context
		/// </summary>
		private static GameContext _instance;

		/// <summary>
		/// Player context
		/// </summary>
		public PlayerContext PlayerContext { get; private set; } = default;

		/// <summary>
		/// Game context getter using singleton
		/// </summary>
		/// <returns>instance of GameContext</returns>
		public static GameContext GetGameContext()
		{
			_instance ??= new GameContext();
			return _instance;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		private GameContext() : base()
		{
			SceneManager.sceneLoaded += OnSceneLoaded;
			UnityEngine.Application.quitting += Dispose;
		}

		/// <summary>
		/// Player context registration
		/// </summary>
		/// <param name="context">player context</param>
		public void RegisterPlayerContext(PlayerContext context)
		{
			if (_includedContexts.Any(x => x is PlayerContext))
			{
				int indexOfContext = _includedContexts.FindIndex(x => x is PlayerContext);
				var previousPlayerContext = (PlayerContext) _includedContexts[indexOfContext];
				_includedContexts[indexOfContext] = context;
				previousPlayerContext.Dispose();
				Debug.Log("Player context changed");
			}
			else
			{
				AddContext(context);
			}
			PlayerContext = context;
		}

		/// <summary>
		/// On scene loaded subscription
		/// </summary>
		/// <param name="scene">loaded scene</param>
		/// <param name="loadMode">load mode</param>
		private void OnSceneLoaded(Scene scene, LoadSceneMode loadMode)
		{
			AddSceneContext(scene);
		}

		/// <summary>
		/// Add scene context
		/// </summary>
		/// <param name="scene">scene</param>
		private void AddSceneContext(Scene scene)
		{
			if (TryCreateSceneContext(scene, out BaseContext context))
			{
				AddContext(context);
				return;
			}
			Debug.LogWarning("Scene doesn't have context");
		}

		/// <summary>
		/// Try create scene context
		/// </summary>
		/// <param name="scene">scene</param>
		/// <param name="context">resulted context</param>
		/// <returns>is success</returns>
		private bool TryCreateSceneContext(Scene scene, out BaseContext context)
		{
			context = null;

			var type = Type.GetType($"{scene.name}Context");
			if (type != null)
			{
				try
				{
					var ctor = type.GetConstructors().Single();
					var paramTypes = ctor.GetParameters().Select(p => p.ParameterType).ToList();
					context = (SceneContext) Activator.CreateInstance(type, new object[] { scene, this });
					return true;
				}
				catch (NotImplementedException ex)
				{
					Debug.LogError(ex.Message);
				}
			}
			else
			{
				Debug.LogError($"Coud't find context for scene {scene.name}");
			}

			return false;
		}

		///<inheritdoc/>
		public override void Dispose()
		{
			SceneManager.sceneLoaded -= OnSceneLoaded;
			_currentContainer.Dispose();
			PlayerContext?.Dispose();
			_instance = null;
			base.Dispose();
		}
	}
}
